import test from 'ava';
import queryTator from '../src';

function wrapExpected(expectedObject) {
  return `?where=${JSON.stringify(expectedObject)}`;
}

test('query().match', t => {
  const query = queryTator().match({ fieldName: 'field', value: 'value' });

  const expected = wrapExpected({ field: 'value' });

  t.is(query.buildQuery(), expected, 'Should return a JSON string of key/value pairs');
});

test('query().match with type', t => {
  const query = queryTator().match({ fieldName: 'field', value: '2016-01-01', type: 'date' });

  const expected = wrapExpected({
    field: {
      $date: '2016-01-01'
    }
  });

  t.is(query.buildQuery(), expected, 'Should return a JSON string of key/value pairs with type');
});

test('query().match dynamic (no value)', t => {
  const query = queryTator().match({ fieldName: 'field' });

  query.setField('field', 'My Value');

  const expected = wrapExpected({ field: 'My Value' });

  t.is(query.buildQuery(), expected,
    'Should return a JSON string of key/value pairs set dynamically');
});

test('query().match dynamic (no value) with type', t => {
  const query = queryTator().match({ fieldName: 'field', type: 'date' });

  query.setField('field', '2016-01-01');

  const expected = wrapExpected({
    field: {
      $date: '2016-01-01'
    }
  });

  t.is(query.buildQuery(), expected,
    'Should return a JSON string of key/value pairs dynamically set with a type');
});

test('query().match and query().match dynamic (no value) together with type', t => {
  const query = queryTator()
    .match({ fieldName: 'dynoField' })
    .match({ fieldName: 'typedDynoField', type: 'date' })
    .match({ fieldName: 'valueField', value: 'valueFieldValue' })
    .match({ fieldName: 'typedField', value: 'typedFieldValue', type: 'string' });

  query
    .setField('dynoField', 'dynoFieldValue')
    .setField('typedDynoField', '2016-01-01');

  const expected = wrapExpected({
    valueField: 'valueFieldValue',
    typedField: {
      $string: 'typedFieldValue'
    },
    dynoField: 'dynoFieldValue',
    typedDynoField: {
      $date: '2016-01-01'
    }
  });

  t.is(query.buildQuery(), expected,
    'Should return a JSON string query match for dynamic and regular');

  query.setField('dynoField', 'secondDynoField');

  const afterChangeExpected = wrapExpected({
    valueField: 'valueFieldValue',
    typedField: {
      $string: 'typedFieldValue'
    },
    dynoField: 'secondDynoField',
    typedDynoField: {
      $date: '2016-01-01'
    }
  });

  t.is(query.buildQuery(), afterChangeExpected,
    'Should return a JSON string query but with updated dynamic field');
});

test('query.conditional', t => {
  const conditionalQuery = queryTator()
    .conditional('gte', { fieldName: 'condProp', limiter: '2016-01-01', type: 'date' })
    .conditional('gte', { fieldName: 'dynamicCondProp' });

  conditionalQuery.setConditional('dynamicCondProp', 60);

  const expected = JSON.stringify({
    condProp: {
      $gte: {
        $date: '2016-01-01'
      }
    },
    dynamicCondProp: {
      $gte: 60
    }
  });

  t.is(conditionalQuery.buildConditional(), expected,
    'Should return a matching JSON string with conditional values set');
});

test('query().or([])', t => {
  const query = queryTator()
    .match({ fieldName: 'fieldMatch', value: 'value' })
    .or([
      queryTator().conditional('gte', {
        fieldName: 'gteFieldName',
        type: 'date',
        limiter: '2016-01-01'
      }),
      queryTator().conditional('gte', {
        fieldName: 'noneTypeConditional',
        limiter: 60
      })
    ]);

  const expected = wrapExpected({
    fieldMatch: 'value',
    $or: [{
      gteFieldName: {
        $gte: {
          $date: '2016-01-01'
        }
      }
    }, {
      noneTypeConditional: {
        $gte: 60
      }
    }]
  });

  t.is(query.buildQuery(), expected,
    'Should return a JSON string in query form with OR conditionals');
});
