function typeHandler(type, value) {
  return {
    [`$${type}`]: value
  };
}

export default function buildKeyValueObject(fieldName, value, type) {
  let valueToSet = value;

  if (type) {
    valueToSet = typeHandler(type, valueToSet);
  }

  return { [fieldName]: valueToSet };
}
