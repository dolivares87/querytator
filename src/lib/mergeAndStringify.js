export default function mergeAndStringify(objectsToMerge) {
  return JSON.stringify(
    Object.assign({}, ...objectsToMerge)
  );
}
