function buildORConditionals(orConditionals = []) {
  return orConditionals.map(cond => {
    const staticConditionals = cond.staticConditionals;

    return Object.assign({}, ...staticConditionals);
  });
}

export default function buildOr(orConditionals = []) {
  const builtConditionals = buildORConditionals(orConditionals);

  return {
    $or: builtConditionals
  };
}
