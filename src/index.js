import 'source-map-support/register';
import mergeAndStringify from './lib/mergeAndStringify';
import buildKeyValueObject from './lib/buildKeyValueObject';
import buildOr from './lib/buildOr';

const query = {
  match({ fieldName, type, value } = {}) {
    if (value) {
      const staticMatcher = buildKeyValueObject(fieldName, value, type);

      this.staticMatchers.push(staticMatcher);

      return this;
    }

    this.dynamicFields.set(fieldName, type || false);

    return this;
  },
  setField(fieldName, value) {
    const dynamicFields = this.dynamicFields;

    if (dynamicFields.has(fieldName)) {
      const type = dynamicFields.get(fieldName);

      this.match({ fieldName, type, value });
    }

    return this;
  },
  conditional(logicalEvaluator, conditionalProps = {}) {
    const { fieldName, limiter, type } = conditionalProps;

    if (limiter) {
      let preppedLimiter = limiter;
      if (type) {
        preppedLimiter = {
          [`$${type}`]: limiter
        };
      }

      const typedConditional = {
        [`$${logicalEvaluator}`]: preppedLimiter
      };

      this.staticConditionals.push({
        [fieldName]: typedConditional
      });

      return this;
    }

    this.dynamicConditionals.set(fieldName, {
      logicalEvaluator,
      type
    });

    return this;
  },
  setConditional(fieldName, limiter) {
    const dynamicConditionals = this.dynamicConditionals;

    if (dynamicConditionals.has(fieldName)) {
      const { logicalEvaluator, type } = dynamicConditionals.get(fieldName);

      this.conditional(logicalEvaluator, { fieldName, type, limiter });
    }

    return this;
  },
  buildConditional() {
    const conditionals = this.staticConditionals;

    return mergeAndStringify(conditionals);
  },
  or(conditionals = []) {
    this.orConditionals.push(...conditionals);

    return this;
  },
  buildQuery() {
    const matchers = this.staticMatchers;
    let orConditionals = this.orConditionals;

    if (orConditionals.length) {
      orConditionals = buildOr(orConditionals);
    } else {
      orConditionals = {};
    }

    const queryObj = mergeAndStringify([...matchers, orConditionals]);

    return `?where=${queryObj}`;
  },
};

export default function querytator() {
  return Object.assign(Object.create(query), {
    orConditionals: [],
    staticMatchers: [],
    staticConditionals: [],
    dynamicFields: new Map(),
    dynamicConditionals: new Map()
  });
}
