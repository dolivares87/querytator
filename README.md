SCHEDULE-BODHI

A simple configuration tool that creates a query to filter Bodhi-Superagent files/records based on 
certain dates/criteria.

API
import query from 'querytator';

const storeId = '123';

query().
  .match({
    storeId
  })
  .match.dynamic('local_name')
  .or([
    query.conditional.dynamic('sys_created_at', 'gte', 'date'),
    query.conditional.dynamic('local_ctime', 'gte', 'date'),
    query.conditional.dynamic('sys_modified_at', 'gte', 'date'),
    '{sys_modified_at:{$gte:{$date:"somedate"}}}'
  ])
  .finalize();


query().finalize
  Composes your query string and it's values. For example if it has dynamic fields, it will make it
  available with the setField method. It also creates the toQueryJSON method to turn the string into 
  JSON.

query().match
  Takes a simple key/value object to match field to value

query().match.dynamic('field')
  Takes a key that will be given a setter function that will return that query string when converted.
  
  To set a dynamic field:
    query.setField('field', 'THIS IS THE VALUE FIELD');
    query.toQueryJSON();

query().or([])
  Takes an array of either querytator conditional instances or strings.
    Querytator instances are independent query builders. Each one will be finalized. Within an OR, 
    the PARENT querytator instance will inherit the setFields for any dynamic conditional.
    
  A string can be any string you pass it. Full trust that it is a valid conditional mongo query rests on the user.

query().conditional('propName', 'logicalConditional', 'type', value)
  Returns a conditional based on the options sent.
